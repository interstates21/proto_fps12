﻿using UnityEngine;

public class PlayerUI : MonoBehaviour {
    [SerializeField]
    RectTransform thrusterFuelFill;
    private PlayerController controller;

    public void SetController (PlayerController _controller)
    {
        controller = _controller;
    }

    private void Update()
    {
        SetFuelAmount(controller.GetFuelAmount());
    }
    void SetFuelAmount(float _amount)
    {
        thrusterFuelFill.localScale = new Vector3(1f, _amount, 1f);
    }
}
